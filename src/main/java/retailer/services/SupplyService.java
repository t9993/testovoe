package retailer.services;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import retailer.domain.SupplyEntity;
import retailer.dto.ReportDto;
import retailer.repos.SupplyRepos;
import retailer.requests.CreateReport;
import retailer.requests.CreateSupply;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


@Service
public class SupplyService {

    @Autowired
    private SessionFactory sessionFactory;

    private final SupplyRepos supplyRepos;


    public SupplyService(SupplyRepos supplyRepos) {
        this.supplyRepos = supplyRepos;
    }

    public SupplyEntity createSupplyEntity(CreateSupply createSupply) {
        Integer totalPrice = (createSupply.getOffersList().getCost() * createSupply.getAmount()) / 100;
        SupplyEntity supplyEntity = new SupplyEntity(createSupply.getDate(), createSupply.getOffersList(), createSupply.getAmount(), totalPrice);

        return supplyRepos.save(supplyEntity);
    }

    public List<SupplyEntity> createSupplyEntityList(List<CreateSupply> createSupply) {
        List<SupplyEntity> supplyEntityList = new ArrayList<>();
        for (CreateSupply supply : createSupply) {
            Integer totalPrice = (supply.getOffersList().getCost() * supply.getAmount()) / 100;
            SupplyEntity supplyEntity = new SupplyEntity(supply.getDate(), supply.getOffersList(), supply.getAmount(), totalPrice);
            supplyEntityList.add(supplyEntity);
        }
        return supplyEntityList;
    }

    public List<ReportDto> createReport(CreateReport createReport) throws ParseException {
        List<ReportDto> reportDtoList = new ArrayList<>();
        List<ReportDto> bufferArray = new ArrayList<>();

        Session session = sessionFactory.openSession();

        String sql;
        Query query;

        sql = "select supplys.date, suppliers.name_supplier,  products.name_product, offers.cost, supplys.total_cost  from suppliers \n" +
                "INNER JOIN suppliers_production ON suppliers.id = suppliers_production.suppliers_id\n" +
                "LEFT JOIN offers ON suppliers_production.offer_id = offers.id\n" +
                "LEFT JOIN products ON offers.id_product = products.id\n" +
                "LEFT JOIN supplys ON supplys.id_offer = offers.id \n" +
                "WHERE supplys.total_cost IS NOT NULL";
        query = session.createSQLQuery(sql);
        List<Object[]> all = query.list();
        for (Object[] row : all) {

            String dateTime = row[0].toString();
            int index = dateTime.indexOf(' ');

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date date1 = sdf.parse(dateTime.substring(0, index));
            if (date1.getTime() >= createReport.getDataStart().getTime()
                    && date1.getTime() <= createReport.getDataFinish().getTime()) {

                ReportDto report = new ReportDto(date1, row[1].toString(), row[2].toString());
                ReportDto bufferReport = new ReportDto(date1, row[1].toString(), row[2].toString(), Integer.valueOf(row[3].toString()), Integer.valueOf(row[4].toString()));
                bufferArray.add(bufferReport);
                int count = 0;
                for (ReportDto reportDto : reportDtoList) {
                    if (Objects.equals(reportDto.getProduct(), report.getProduct())
                            && Objects.equals(reportDto.getNameSupplier(), report.getNameSupplier())) {
                        count++;
                    }
                }
                if (count == 0) {
                    reportDtoList.add(report);
                }
            }
        }

        List<ReportDto> list = new ArrayList(reportDtoList);
        for (ReportDto reportDto : list) {
            String supplier = reportDto.getNameSupplier();
            String production = reportDto.getProduct();
            Integer sumCost = 0, sumAmount = 0;
            for (ReportDto dto : bufferArray) {
                if (Objects.equals(dto.getProduct(), production) && Objects.equals(supplier, dto.getNameSupplier())) {
                    sumCost += dto.getSumCost();
                    sumAmount += dto.getAmount();
                }
            }
            reportDto.setSumCost(sumCost);
            reportDto.setAmount(sumAmount);
        }

        return list;
    }
}
