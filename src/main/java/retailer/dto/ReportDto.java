package retailer.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

@Data
@AllArgsConstructor
public class ReportDto {
    private Date date;
    private String nameSupplier;
    private String product;
    private Integer amount;
    private Integer sumCost;

    public ReportDto(String nameSupplier) {
        this.nameSupplier = nameSupplier;
    }

    public ReportDto() {

    }

    public ReportDto(Date date, String nameSupplier, String product) {
        this.date = date;
        this.nameSupplier = nameSupplier;
        this.product = product;
    }

}
