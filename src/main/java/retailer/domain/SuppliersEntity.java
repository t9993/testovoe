package retailer.domain;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "suppliers")
public class SuppliersEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String nameSupplier;
    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "supplier")
    private Set<OfferEntity> offerEntity;

    public SuppliersEntity(Long id, String nameSupplier) {
        this.id = id;
        this.nameSupplier = nameSupplier;
    }

    public Set<OfferEntity> getOfferEntity() {
        return offerEntity;
    }

    public void setOfferEntity(Set<OfferEntity> offerEntity) {
        this.offerEntity = offerEntity;
    }

    public SuppliersEntity(String nameSupplier) {
        this.nameSupplier = nameSupplier;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNameSupplier() {
        return nameSupplier;
    }

    public void setNameSupplier(String nameSupplier) {
        this.nameSupplier = nameSupplier;
    }
}
