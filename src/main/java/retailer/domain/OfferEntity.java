package retailer.domain;

import lombok.*;

import javax.persistence.*;
import java.util.Set;


@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "offers")
public class OfferEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Integer cost;
    @ManyToMany
    @JoinTable(name = "suppliers_production",
            joinColumns = @JoinColumn(name = "offer_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "suppliers_id", referencedColumnName = "id")
    )
    private Set<SuppliersEntity> supplier;
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "offersList")
    private Set<SupplyEntity> offers;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_product", nullable = false)
    private ProductsEntity products;

    public OfferEntity(Integer cost, Set<SuppliersEntity> supplier, ProductsEntity products) {
        this.cost = cost;
        this.supplier = supplier;
        this.products = products;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }

    public Set<SuppliersEntity> getSupplier() {
        return supplier;
    }

    public void setSupplier(Set<SuppliersEntity> supplier) {
        this.supplier = supplier;
    }

    public Set<SupplyEntity> getOffers() {
        return offers;
    }

    public void setOffers(Set<SupplyEntity> offers) {
        this.offers = offers;
    }

    public ProductsEntity getProducts() {
        return products;
    }

    public void setProducts(ProductsEntity products) {
        this.products = products;
    }
}
