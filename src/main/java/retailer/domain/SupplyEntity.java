
package retailer.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;


@Entity
@AllArgsConstructor
@Table(name = "supplys")
public class SupplyEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private Date date;
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE})
    @JoinColumn(name = "id_offer", nullable = false)
    private OfferEntity offersList;

    private Integer amount;
    private Integer totalCost;

    public SupplyEntity() {
    }

    public SupplyEntity(Date date, OfferEntity offersList, Integer amount, Integer totalCost) {
        this.date = date;
        this.offersList = offersList;
        this.amount = amount;
        this.totalCost = totalCost;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public OfferEntity getOffersList() {
        return offersList;
    }

    public void setOffersList(OfferEntity offersList) {
        this.offersList = offersList;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(Integer totalCost) {
        this.totalCost = totalCost;
    }
}

