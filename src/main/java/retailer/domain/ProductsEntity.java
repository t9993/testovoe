
package retailer.domain;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "products")
public class ProductsEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String nameProduct;
    private String typeProduct;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "products")
    private List<OfferEntity> offer;

    public ProductsEntity(Long id, String nameProduct, String typeProduct) {
        this.id = id;
        this.nameProduct = nameProduct;
        this.typeProduct = typeProduct;
    }

    public ProductsEntity(String nameProduct, String typeProduct) {
        this.nameProduct = nameProduct;
        this.typeProduct = typeProduct;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNameProduct() {
        return nameProduct;
    }

    public void setNameProduct(String nameProduct) {
        this.nameProduct = nameProduct;
    }

    public String getTypeProduct() {
        return typeProduct;
    }

    public void setTypeProduct(String typeProduct) {
        this.typeProduct = typeProduct;
    }

    public List<OfferEntity> getOffer() {
        return offer;
    }

    public void setOffer(List<OfferEntity> offer) {
        this.offer = offer;
    }
}

