package retailer.controllers;

import org.springframework.web.bind.annotation.*;
import retailer.domain.*;
import retailer.dto.ReportDto;
import retailer.repos.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import retailer.requests.*;
import retailer.services.SupplyService;

import java.text.ParseException;
import java.util.List;


@RequestMapping("/entity")
@Controller
public class EntityController {
    @Autowired
    private ProductRepos productRepos;
    @Autowired
    private SuppliersRepos suppliersRepos;
    @Autowired
    private SupplyRepos supplyRepos;
    @Autowired
    private OfferRepos offerRepos;

    private final SupplyService supplyService;

    public EntityController(SupplyService supplyService) {
        this.supplyService = supplyService;
    }

    @PostMapping("/products/add")
    public @ResponseBody
    ProductsEntity productsAdd(@RequestBody CreateProduct product) {
        ProductsEntity productsEntity = new ProductsEntity(product.getNameProduct(), product.getTypeProduct());
        return productRepos.save(productsEntity);
    }

    @PostMapping("/offer/add")
    public @ResponseBody
    OfferEntity offerAdd(@RequestBody CreateOffer offer) {
        OfferEntity offerEntity = new OfferEntity(offer.getCost(), offer.getEntityList(), offer.getProducts());
        return offerRepos.save(offerEntity);
    }

    @PostMapping("/suppliers/add")
    public @ResponseBody
    SuppliersEntity suppliersAdd(@RequestBody CreateSupplier suppliers) {
        SuppliersEntity suppliersEntity = new SuppliersEntity(suppliers.getNameSupplier());
        return suppliersRepos.save(suppliersEntity);
    }

    @PostMapping("/supplie/add")
    public @ResponseBody
    SupplyEntity supplieAdd(@RequestBody CreateSupply supply) {
        return supplyService.createSupplyEntity(supply);
    }

    @PostMapping("/supplie/addList")
    public @ResponseBody
    List<SupplyEntity> supplieAdd(@RequestBody List<CreateSupply> supply) {
        return supplyService.createSupplyEntityList(supply);
    }

    @GetMapping("/report")
    public @ResponseBody
    List<ReportDto> report(@RequestBody CreateReport createReport) throws ParseException {
        return supplyService.createReport(createReport);
    }

}


