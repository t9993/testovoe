package retailer.repos;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import retailer.domain.ProductsEntity;

@Repository
public interface ProductRepos extends CrudRepository<ProductsEntity, Long>{
}
