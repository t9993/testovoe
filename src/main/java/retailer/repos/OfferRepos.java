package retailer.repos;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import retailer.domain.OfferEntity;

@Repository
public interface OfferRepos extends CrudRepository<OfferEntity, Long> {
}
