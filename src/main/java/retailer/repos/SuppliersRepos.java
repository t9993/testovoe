package retailer.repos;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import retailer.domain.SuppliersEntity;

@Repository
public interface SuppliersRepos extends CrudRepository<SuppliersEntity, Long> {
}
