package retailer.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import retailer.domain.SupplyEntity;


@Repository
public interface SupplyRepos extends CrudRepository<SupplyEntity, Long> {
}
