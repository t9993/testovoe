package retailer.requests;

import lombok.Data;
import retailer.domain.OfferEntity;

import java.util.Date;
@Data
public class CreateSupply {
    private Date date;
    private OfferEntity offersList;
    private Integer amount;
}
