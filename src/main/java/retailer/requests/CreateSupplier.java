package retailer.requests;

import lombok.Data;

@Data
public class CreateSupplier {
    private String nameSupplier;
}
