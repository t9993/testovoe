package retailer.requests;

import lombok.Data;

import java.util.Date;

@Data
public class CreateReport {
    private Date dataStart;
    private Date dataFinish;
}
