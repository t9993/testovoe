package retailer.requests;

import lombok.Data;

@Data
public class CreateProduct {
    private String nameProduct;
    private String typeProduct;
}
