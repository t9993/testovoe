package retailer.requests;

import lombok.Data;
import retailer.domain.ProductsEntity;
import retailer.domain.SuppliersEntity;

import java.util.Set;

@Data
public class CreateOffer {
    private Integer cost;
    private Set<SuppliersEntity> entityList;
    private ProductsEntity products;
}
